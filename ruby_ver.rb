require "sinatra"
require "cf-autoconfig"

class RubyVer < Sinatra::Base
  get "/" do
    "Ruby #{RUBY_VERSION}-p#{RUBY_PATCHLEVEL}\n"
  end

  get "/env" do
    ENV.to_hash.to_s + "\n"
  end
end
