ruby-version
============
An example application for Cloud Foundry v2 environment.

Tested with Ruby v2.0.0.

Usage
-----
Access

- '/': show the version and patchlevel of ruby of the running environmtent
- '/env': show the environment variables of the running environmtent
